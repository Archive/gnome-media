# Lithuanian translation of gnome-media
# Copyright © 2000-2010 Free Software Foundation, Inc.
# Gediminas Paulauskas <menesis@chatsubo.lt>, 2000-2003.
# Žygimantas Beručka <zygis@gnome.org>, 2003-2006, 2009, 2010.
# Justina Klingaitė <justina.klingaite@gmail.com>, 2005.
# Gintautas Miliauskas <gintas@akl.lt>, 2007.
# Rimas Kudelis <rq@akl.lt>, 2009.
# Aurimas Černius <aurisc4@gmail.com>, 2010.
msgid ""
msgstr ""
"Project-Id-Version: lt\n"
"Report-Msgid-Bugs-To: http://bugzilla.gnome.org/enter_bug.cgi?product=gnome-media&keywords=I18N+L10N&component=general\n"
"POT-Creation-Date: 2012-06-06 11:39+0000\n"
"PO-Revision-Date: 2012-08-05 16:49+0300\n"
"Last-Translator: Aurimas Černius <aurisc4@gmail.com>\n"
"Language-Team: Lithuanian <gnome-lt@lists.akl.lt>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: lt\n"
"Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && (n%100<10 || n%100>=20) ? 1 : 2);\n"
"X-Generator: Virtaal 0.6.1\n"

#: ../grecord/gnome-sound-recorder.desktop.in.in.h:1
#: ../grecord/src/gsr-window.c:1226
msgid "Sound Recorder"
msgstr "Garso įrašymo programa"

#: ../grecord/gnome-sound-recorder.desktop.in.in.h:2
msgid "Record sound clips"
msgstr "Rašyti garso įrašus"

#: ../grecord/src/gsr-window.c:194
#, c-format
msgid ""
"Could not create the GStreamer %s element.\n"
"Please install the '%s' plugin from the '%s' module.\n"
"Verify that the installation is correct by running\n"
"    gst-inspect-0.10 %s\n"
"and then restart gnome-sound-recorder."
msgstr ""
"Nepavyko sukurti GStreamer elemento %s.\n"
"Įdiekite įskiepį „%s“ iš modulio „%s“.\n"
"Patikrinkite, ar įdiegta korektiškai, paleidę\n"
"    gst-inspect-0.10 %s\n"
"ir tada perleiskite gnome-sound-recorder."

#: ../grecord/src/gsr-window.c:210
msgid ""
"Please verify its settings.\n"
"You may be missing the necessary plugins."
msgstr ""
"Patikrinkite jo nustatymus.\n"
"Galbūt neturite reikalingų įskiepių."

#: ../grecord/src/gsr-window.c:295
#, c-format
msgid "%ld minute"
msgid_plural "%ld minutes"
msgstr[0] "%ld minutė"
msgstr[1] "%ld minutės"
msgstr[2] "%ld minučių"

#: ../grecord/src/gsr-window.c:296
#, c-format
msgid "%ld hour"
msgid_plural "%ld hours"
msgstr[0] "%ld valanda"
msgstr[1] "%ld valandos"
msgstr[2] "%ld valandų"

#: ../grecord/src/gsr-window.c:297
#, c-format
msgid "%ld second"
msgid_plural "%ld seconds"
msgstr[0] "%ld sekundė"
msgstr[1] "%ld sekundės"
msgstr[2] "%ld sekundžių"

#. Translators: the format is "X hours, X minutes and X seconds"
#: ../grecord/src/gsr-window.c:304
#, c-format
msgid "%s, %s and %s"
msgstr "%s, %s ir %s"

#. Translators: the format is "X hours and X minutes"
#. Translators: the format is "X minutes and X seconds"
#: ../grecord/src/gsr-window.c:310
#: ../grecord/src/gsr-window.c:318
#: ../grecord/src/gsr-window.c:329
#, c-format
msgid "%s and %s"
msgstr "%s ir %s"

#: ../grecord/src/gsr-window.c:372
msgid "Open a File"
msgstr "Atverti failą"

#: ../grecord/src/gsr-window.c:449
#, c-format
msgid ""
"Unable to load file:\n"
"%s"
msgstr ""
"Nepavyko įkelti failo:\n"
"%s"

#: ../grecord/src/gsr-window.c:595
#: ../grecord/src/gsr-window.c:1528
#: ../grecord/src/gsr-window.c:1796
#: ../grecord/src/gsr-window.c:2498
msgid "Ready"
msgstr "Paruošta"

#. Add replace button
#: ../grecord/src/gsr-window.c:648
msgid "_Replace"
msgstr "_Pakeisti"

#: ../grecord/src/gsr-window.c:665
#, c-format
msgid ""
"A file named \"%s\" already exists. \n"
"Do you want to replace it with the one you are saving?"
msgstr ""
"Failas pavadinimu „%s“ jau egzistuoja.\n"
"Ar norite jį perrašyti?"

#: ../grecord/src/gsr-window.c:713
#, c-format
msgid "Could not save the file \"%s\""
msgstr "Nepavyko įrašyti failo „%s“"

#: ../grecord/src/gsr-window.c:733
msgid "Save file as"
msgstr "Įrašyti failą kaip"

#: ../grecord/src/gsr-window.c:816
#, c-format
msgid "%s is not installed in the path."
msgstr "%s nėra įdiegtas kelyje."

#: ../grecord/src/gsr-window.c:825
#, c-format
msgid "There was an error starting %s: %s"
msgstr "Paleidžiant %s įvyko klaida: %s"

#: ../grecord/src/gsr-window.c:853
msgid "Save recording before closing?"
msgstr "Ar išsaugoti įrašą prieš užveriant?"

#: ../grecord/src/gsr-window.c:854
msgid "Save recording?"
msgstr "Ar išsaugoti įrašą?"

#: ../grecord/src/gsr-window.c:858
msgid "Close _without Saving"
msgstr "Užverti _nesaugant"

#: ../grecord/src/gsr-window.c:859
msgid "Continue _without Saving"
msgstr "Tęsti _nesaugant"

#: ../grecord/src/gsr-window.c:869
msgid "Question"
msgstr "Klausimas"

#: ../grecord/src/gsr-window.c:976
#, c-format
msgid "%s (Has not been saved)"
msgstr "%s (neišsaugotas)"

#: ../grecord/src/gsr-window.c:992
msgid "%s (%"
msgid_plural "%s (%"
msgstr[0] "%s (%"
msgstr[1] "%s (%"
msgstr[2] "%s (%"

#: ../grecord/src/gsr-window.c:996
msgid "Unknown size"
msgstr "Nežinomas dydis"

#. Attempts to get length ran out.
#: ../grecord/src/gsr-window.c:1005
#: ../grecord/src/gsr-window.c:1015
#: ../grecord/src/gsr-window.c:1031
#: ../grecord/src/gsr-window.c:1040
#: ../grecord/src/gsr-window.c:1396
#: ../gstreamer-properties/gstreamer-properties.c:302
msgid "Unknown"
msgstr "Nežinomas"

#: ../grecord/src/gsr-window.c:1017
#, c-format
msgid "%.1f kHz"
msgstr "%.1f kHz"

#: ../grecord/src/gsr-window.c:1025
#, c-format
msgid "%.0f kb/s"
msgstr "%.0f kb/s"

#: ../grecord/src/gsr-window.c:1028
#, c-format
msgid "%.0f kb/s (Estimated)"
msgstr "%.0f kb/s (apytiksliai)"

#: ../grecord/src/gsr-window.c:1043
msgid "1 (mono)"
msgstr "1 (mono)"

#: ../grecord/src/gsr-window.c:1046
msgid "2 (stereo)"
msgstr "2 (stereo)"

#: ../grecord/src/gsr-window.c:1073
#, c-format
msgid "%s Information"
msgstr "%s informacija"

#: ../grecord/src/gsr-window.c:1096
#: ../grecord/src/gsr-window.c:2410
msgid "File Information"
msgstr "Failo informacija"

#: ../grecord/src/gsr-window.c:1111
msgid "Folder:"
msgstr "Aplankas:"

#: ../grecord/src/gsr-window.c:1117
#: ../grecord/src/gsr-window.c:2417
msgid "Filename:"
msgstr "Failo vardas:"

#: ../grecord/src/gsr-window.c:1123
msgid "File size:"
msgstr "Failo dydis:"

#: ../grecord/src/gsr-window.c:1132
msgid "Audio Information"
msgstr "Garso informacija"

#: ../grecord/src/gsr-window.c:1147
msgid "File duration:"
msgstr "Failo trukmė:"

#: ../grecord/src/gsr-window.c:1153
msgid "Number of channels:"
msgstr "Kanalų kiekis:"

#: ../grecord/src/gsr-window.c:1159
msgid "Sample rate:"
msgstr "Semplo dažnis:"

#: ../grecord/src/gsr-window.c:1165
msgid "Bit rate:"
msgstr "Bitų dažnis:"

#: ../grecord/src/gsr-window.c:1229
msgid ""
"A sound recorder for GNOME\n"
" gnome-multimedia@gnome.org"
msgstr ""
"GNOME garso įrašymo programa\n"
" gnome-multimedia@gnome.org"

#: ../grecord/src/gsr-window.c:1500
msgid "Playing…"
msgstr "Atkuriama…"

#: ../grecord/src/gsr-window.c:1632
msgid "GConf audio output"
msgstr "GConf garso išvestis"

#: ../grecord/src/gsr-window.c:1641
#: ../gst-mixer/src/misc.c:62
msgid "Playback"
msgstr "Grojimas"

#. Translators: this is the window title, %s is the currently open file's name or Untitled
#: ../grecord/src/gsr-window.c:1722
#: ../grecord/src/gsr-window.c:2571
#, c-format
msgid "%s — Sound Recorder"
msgstr "%s — Garso įrašymo programa"

#: ../grecord/src/gsr-window.c:1742
msgid "Recording…"
msgstr "Įrašoma…"

#: ../grecord/src/gsr-window.c:1818
msgid "GConf audio recording"
msgstr "GConf garso įrašymas"

#: ../grecord/src/gsr-window.c:1827
msgid "Your audio capture settings are invalid. Please correct them with the \"Sound Preferences\" under the System Preferences menu."
msgstr "Jūsų garso įrašymo nustatymai netinkami. Pataisykite juos „Garso nustatymuose\", kuriuos galite rasti meniu Sistema → Nustatymai."

#: ../grecord/src/gsr-window.c:1999
msgid "file output"
msgstr "failo išvestis"

#: ../grecord/src/gsr-window.c:2017
msgid "level"
msgstr "lygmuo"

#: ../grecord/src/gsr-window.c:2041
#, c-format
msgid "Could not parse the '%s' audio profile. "
msgstr "Nepavyko apdoroti „%s“ garso profilio. "

#: ../grecord/src/gsr-window.c:2058
#, c-format
msgid "Could not capture using the '%s' audio profile. "
msgstr "Nepavyko įrašyti naudojant „%s“ garso profilį. "

#: ../grecord/src/gsr-window.c:2067
#, c-format
msgid "Could not write to a file using the '%s' audio profile. "
msgstr "Nepavyko įrašyti į failą naudojant „%s“ garso profilį. "

#. File menu.
#: ../grecord/src/gsr-window.c:2121
msgid "_File"
msgstr "_Failas"

#: ../grecord/src/gsr-window.c:2123
msgid "Create a new sample"
msgstr "Sukurti naują garso elementą"

#: ../grecord/src/gsr-window.c:2125
msgid "Open a file"
msgstr "Atverti failą"

#: ../grecord/src/gsr-window.c:2127
msgid "Save the current file"
msgstr "Įrašyti esamą failą"

#: ../grecord/src/gsr-window.c:2129
msgid "Save the current file with a different name"
msgstr "Įrašyti esamą failą kitu vardu"

#: ../grecord/src/gsr-window.c:2130
msgid "Open Volu_me Control"
msgstr "Atverti garsu_mo valdymą"

#: ../grecord/src/gsr-window.c:2131
msgid "Open the audio mixer"
msgstr "Atverti garso maišiklį"

#: ../grecord/src/gsr-window.c:2133
msgid "Show information about the current file"
msgstr "Rodyti esamo failo informaciją"

#: ../grecord/src/gsr-window.c:2135
msgid "Close the current file"
msgstr "Užverti esamą failą"

#: ../grecord/src/gsr-window.c:2137
msgid "Quit the program"
msgstr "Užverti programą"

#. Control menu
#: ../grecord/src/gsr-window.c:2140
msgid "_Control"
msgstr "_Valdymas"

#: ../grecord/src/gsr-window.c:2142
msgid "Record sound"
msgstr "Įrašyti garsą"

#: ../grecord/src/gsr-window.c:2144
msgid "Play sound"
msgstr "Atkurti garsą"

#: ../grecord/src/gsr-window.c:2146
msgid "Stop sound"
msgstr "Stabdyti garsą"

#. Help menu
#: ../grecord/src/gsr-window.c:2149
msgid "_Help"
msgstr "_Žinynas"

#: ../grecord/src/gsr-window.c:2150
msgid "Contents"
msgstr "Turinys"

#: ../grecord/src/gsr-window.c:2151
msgid "Open the manual"
msgstr "Atverti žinyną"

#: ../grecord/src/gsr-window.c:2153
msgid "About this application"
msgstr "Apie šią programą"

#: ../grecord/src/gsr-window.c:2275
msgid "Could not load UI file. The program may not be properly installed."
msgstr "Nepavyko įkelti grafinės sąsajos failo. Gali būti, kad programa tinkamai neįdiegta."

#: ../grecord/src/gsr-window.c:2298
msgid "Open"
msgstr "Atverti"

#: ../grecord/src/gsr-window.c:2300
msgid "Save"
msgstr "Išsaugoti"

#: ../grecord/src/gsr-window.c:2302
msgid "Save As"
msgstr "Išsaugoti kaip"

#: ../grecord/src/gsr-window.c:2359
msgid "Record from _input:"
msgstr "Įrašyti iš į_vesties:"

#: ../grecord/src/gsr-window.c:2377
msgid "_Record as:"
msgstr "Įrašyti _kaip:"

#: ../grecord/src/gsr-window.c:2424
msgid "<none>"
msgstr "<nėra>"

#: ../grecord/src/gsr-window.c:2438
msgid "Length:"
msgstr "Trukmė:"

#: ../grecord/src/gsr-window.c:2476
msgid "Level:"
msgstr "Lygmuo:"

#. Translator comment: default trackname is 'untitled', which
#. * has as effect that the user cannot save to this file. The
#. * 'save' action will open the save-as dialog instead to give
#. * a proper filename. See gnome-record.c:94.
#: ../grecord/src/gsr-window.c:2687
msgid "Untitled"
msgstr "Nepavadinta"

#: ../gst-mixer/gnome-volume-control.desktop.in.in.h:1
#: ../gst-mixer/src/window.c:269
#: ../gst-mixer/src/window.c:270
msgid "Volume Control"
msgstr "Garso valdymas"

#: ../gst-mixer/gnome-volume-control.desktop.in.in.h:2
msgid "Change sound volume and sound events"
msgstr "Keisti garsumą ir susieti garsus su įvykiais"

#: ../gst-mixer/gnome-volume-control.schemas.in.h:1
msgid "Height of the Window"
msgstr "Lango aukštis"

#: ../gst-mixer/gnome-volume-control.schemas.in.h:2
msgid "Height of the window to be displayed."
msgstr "Rodomo lango dydis."

#: ../gst-mixer/gnome-volume-control.schemas.in.h:3
msgid "Width of the Window"
msgstr "Lango plotis"

#: ../gst-mixer/gnome-volume-control.schemas.in.h:4
msgid "Width of the window to be displayed."
msgstr "Rodomo lango plotis."

#: ../gst-mixer/src/element.c:495
msgid "Sound Theme"
msgstr "Garsų tema"

#: ../gst-mixer/src/main.c:39
msgid "Startup page"
msgstr "Pradžios puslapis"

#: ../gst-mixer/src/main.c:82
#, c-format
msgid "Unknown Volume Control %d"
msgstr "Nežinomas garso valdymo elementas %d"

#: ../gst-mixer/src/main.c:159
msgid "No volume control GStreamer plugins and/or devices found."
msgstr "Garso valdymo GStreamer įskiepių ir / arba įrenginių nerasta."

#: ../gst-mixer/src/misc.c:64
msgid "Recording"
msgstr "Įrašoma"

#: ../gst-mixer/src/misc.c:66
msgid "Switches"
msgstr "Jungikliai"

#: ../gst-mixer/src/misc.c:68
msgid "Options"
msgstr "Parinktys"

#. make window look cute
#: ../gst-mixer/src/preferences.c:107
msgid "Volume Control Preferences"
msgstr "Garso valdymo nustatymai"

#: ../gst-mixer/src/preferences.c:120
msgid "_Select mixers to be visible:"
msgstr "_Pasirinkite rodytinus maišiklius:"

#: ../gst-mixer/src/track.c:295
#, c-format
msgid "%s:"
msgstr "%s:"

#. mute button
#: ../gst-mixer/src/track.c:339
#, c-format
msgid "Mute/Unmute %s"
msgstr "Nutildyti / netildyti %s"

#: ../gst-mixer/src/track.c:356
#, c-format
msgid "Track %s: mute"
msgstr "Takelis %s: nutildyta"

#. only the record button here
#: ../gst-mixer/src/track.c:433
#, c-format
msgid "Toggle audio recording from %s"
msgstr "Įjungti / išjungti garso rašymą iš %s"

#: ../gst-mixer/src/track.c:449
#, c-format
msgid "Track %s: audio recording"
msgstr "Takelis %s: garso rašymas"

#: ../gst-mixer/src/track.c:562
#, c-format
msgid "%s Option Selection"
msgstr "%s parinkčių parinkimas"

#: ../gst-mixer/src/volume.c:112
#, c-format
msgid "Track %s"
msgstr "Takelis %s"

#: ../gst-mixer/src/volume.c:115
#, c-format
msgid "Channel %d of track %s"
msgstr "%2$s takelio %1$d kanalas"

#: ../gst-mixer/src/volume.c:118
#, c-format
msgid "Track %s, channel %d"
msgstr "Takelis %s, kanalas %d"

#: ../gst-mixer/src/volume.c:138
#, c-format
msgid "Lock channels for %s together"
msgstr "Užrakinti %s kanalus kartu"

#: ../gst-mixer/src/volume.c:156
#, c-format
msgid "Track %s: lock channels together"
msgstr "Takelis %s: užrakinti kanalus kartu"

#: ../gst-mixer/src/volume.c:201
msgid "mono"
msgstr "mono"

#: ../gst-mixer/src/volume.c:203
msgid "left"
msgstr "kairė"

#: ../gst-mixer/src/volume.c:203
msgid "right"
msgstr "dešinė"

#: ../gst-mixer/src/volume.c:206
msgid "front left"
msgstr "priekinė kairė"

#: ../gst-mixer/src/volume.c:207
msgid "front right"
msgstr "priekinė dešinė"

#: ../gst-mixer/src/volume.c:208
msgid "rear left"
msgstr "galinė kairė"

#: ../gst-mixer/src/volume.c:209
msgid "rear right"
msgstr "galinė dešinė"

#: ../gst-mixer/src/volume.c:210
msgid "front center"
msgstr "vidurinė priekinė"

#. Translators: This is the name of a surround sound channel. It
#. * stands for "Low-Frequency Effects". If you're not sure that
#. * this has an established and different translation in your
#. * language, leave it unchanged.
#: ../gst-mixer/src/volume.c:215
msgid "LFE"
msgstr "LFE"

#: ../gst-mixer/src/volume.c:216
msgid "side left"
msgstr "šoninė kairė"

#: ../gst-mixer/src/volume.c:217
msgid "side right"
msgstr "šoninė dešinė"

#: ../gst-mixer/src/volume.c:218
msgid "unknown"
msgstr "nežinoma"

#. Here, we can actually tell people that this
#. * is a slider that will change channel X.
#: ../gst-mixer/src/volume.c:224
#, c-format
msgid "Volume of %s channel on %s"
msgstr "%s kanalo, esančio %s, garsumas"

#: ../gst-mixer/src/window.c:133
msgid "A GNOME/GStreamer-based volume control application"
msgstr "GNOME / GStreamer sistema paremta garso valdymo programa"

#: ../gst-mixer/src/window.c:136
msgid "translator-credits"
msgstr ""
"Paskutinieji vertėjai:\n"
"Justina Klingaitė <justina.klingaite@gmail.com>,\n"
"Žygimantas Beručka <zygis@gnome.org>\n"
"Gediminas Paulauskas <menesis@chatsubo.lt>"

#. change window title
#: ../gst-mixer/src/window.c:173
#, c-format
msgid "Volume Control: %s"
msgstr "Garsumo valdymas: %s"

#: ../gst-mixer/src/window.c:378
msgid "_Device: "
msgstr "Į_renginys: "

#. set tooltips
#: ../gst-mixer/src/window.c:417
msgid "Control volume on a different device"
msgstr "Valdyti garsumą kitame įrenginyje"

#: ../gstreamer-properties/gstreamer-properties.c:205
msgid "None"
msgstr "Joks"

#. Element does not support setting devices
#: ../gstreamer-properties/gstreamer-properties.c:247
msgid "Unsupported"
msgstr "Nepalaikomas"

#: ../gstreamer-properties/gstreamer-properties.c:269
#: ../sound-theme/gvc-sound-theme-chooser.c:818
#: ../sound-theme/gvc-sound-theme-editor.c:891
#: ../sound-theme/gvc-sound-theme-editor.c:995
msgid "Default"
msgstr "Numatytasis"

#: ../gstreamer-properties/gstreamer-properties.c:626
msgid "Failure instantiating main window"
msgstr "Nepavyko parodyti pagrindinio lango"

#: ../gstreamer-properties/gstreamer-properties.c:679
msgid "Failed to load UI file; please check your installation."
msgstr "Nepavyko įkelti naudotojo sąsajos failo. Patinkrite, ar programa gerai įdiegta."

#: ../gstreamer-properties/gstreamer-properties.desktop.in.in.h:1
#: ../gstreamer-properties/gstreamer-properties.ui.h:1
msgid "Multimedia Systems Selector"
msgstr "Multimedijos sistemų konfigūravimas"

#: ../gstreamer-properties/gstreamer-properties.desktop.in.in.h:2
msgid "Configure defaults for GStreamer applications"
msgstr "Keisti numatytuosius GStreamer programų nustatymus"

#: ../gstreamer-properties/gstreamer-properties.ui.h:2
msgid "Default Output"
msgstr "Numatytoji išvestis"

#: ../gstreamer-properties/gstreamer-properties.ui.h:3
msgid "_Plugin:"
msgstr "Į_skiepis:"

#: ../gstreamer-properties/gstreamer-properties.ui.h:4
msgid "P_ipeline:"
msgstr "_Konvejeris:"

#: ../gstreamer-properties/gstreamer-properties.ui.h:5
msgid "_Test"
msgstr "_Tikrinti"

#: ../gstreamer-properties/gstreamer-properties.ui.h:6
msgid "_Device:"
msgstr "Į_renginys:"

#: ../gstreamer-properties/gstreamer-properties.ui.h:7
msgid "Default Input"
msgstr "Numatytoji įvestis"

#: ../gstreamer-properties/gstreamer-properties.ui.h:8
msgid "P_lugin:"
msgstr "Į_skiepis:"

#: ../gstreamer-properties/gstreamer-properties.ui.h:9
msgid "Pipeli_ne:"
msgstr "Kon_vejeris:"

#: ../gstreamer-properties/gstreamer-properties.ui.h:10
msgid "Te_st"
msgstr "_Tikrinti"

#: ../gstreamer-properties/gstreamer-properties.ui.h:11
msgid "D_evice:"
msgstr "Į_renginys:"

#: ../gstreamer-properties/gstreamer-properties.ui.h:12
msgid "Audio"
msgstr "Garsas"

#: ../gstreamer-properties/gstreamer-properties.ui.h:13
msgid "Video"
msgstr "Vaizdas"

#: ../gstreamer-properties/gstreamer-properties.ui.h:14
msgid "Testing Pipeline"
msgstr "Tikrinamas konvejeris"

#: ../gstreamer-properties/gstreamer-properties.ui.h:15
msgid "Testing…"
msgstr "Tikrinama…"

#: ../gstreamer-properties/gstreamer-properties.ui.h:16
msgid "Click Ok to finish."
msgstr "Norėdami baigti, spustelėkite „Gerai“."

#: ../gstreamer-properties/pipeline-constants.c:52
#: ../gstreamer-properties/pipeline-constants.c:90
msgid "Autodetect"
msgstr "Nustatyti automatiškai"

#: ../gstreamer-properties/pipeline-constants.c:54
#: ../gstreamer-properties/pipeline-constants.c:121
msgid "ALSA — Advanced Linux Sound Architecture"
msgstr "ALSA — Advanced Linux Sound Architecture"

#: ../gstreamer-properties/pipeline-constants.c:69
msgid "Artsd — ART Sound Daemon"
msgstr "Artsd — ART garso tarnyba"

#: ../gstreamer-properties/pipeline-constants.c:71
#: ../gstreamer-properties/pipeline-constants.c:123
msgid "ESD — Enlightenment Sound Daemon"
msgstr "ESD — Enlightenment garso tarnyba"

#: ../gstreamer-properties/pipeline-constants.c:77
#: ../gstreamer-properties/pipeline-constants.c:130
msgid "OSS — Open Sound System"
msgstr "OSS — Open Sound System"

#: ../gstreamer-properties/pipeline-constants.c:79
#: ../gstreamer-properties/pipeline-constants.c:132
msgid "OSS - Open Sound System Version 4"
msgstr "OSS – Open Sound System 4-a versija"

#: ../gstreamer-properties/pipeline-constants.c:81
#: ../gstreamer-properties/pipeline-constants.c:136
msgid "PulseAudio Sound Server"
msgstr "PulseAudio garso serveris"

#: ../gstreamer-properties/pipeline-constants.c:83
#: ../gstreamer-properties/pipeline-constants.c:134
msgid "Sun Audio"
msgstr "Sun Audio"

#: ../gstreamer-properties/pipeline-constants.c:85
#: ../gstreamer-properties/pipeline-constants.c:116
#: ../gstreamer-properties/pipeline-constants.c:144
#: ../gstreamer-properties/pipeline-constants.c:159
#: ../sound-theme/gvc-sound-theme-chooser.c:623
#: ../sound-theme/gvc-sound-theme-editor.c:705
#: ../sound-theme/gvc-sound-theme-editor.c:759
#: ../sound-theme/sound-theme-file-utils.c:292
msgid "Custom"
msgstr "Pasirinktinis"

#: ../gstreamer-properties/pipeline-constants.c:108
msgid "OpenGL"
msgstr "OpenGL"

#: ../gstreamer-properties/pipeline-constants.c:110
msgid "SDL — Simple DirectMedia Layer"
msgstr "SDL — Simple DirectMedia Layer"

#: ../gstreamer-properties/pipeline-constants.c:112
msgid "X Window System (No Xv)"
msgstr "X Window System (be Xv)"

#: ../gstreamer-properties/pipeline-constants.c:114
msgid "X Window System (X11/XShm/Xv)"
msgstr "X Window System (X11/XShm/Xv)"

#. Note: using triangle instead of sine for test sound so we
#. * can test the vorbis encoder as well (otherwise it'd compress too well)
#: ../gstreamer-properties/pipeline-constants.c:140
msgid "Test Sound"
msgstr "Bandomasis garsas"

#: ../gstreamer-properties/pipeline-constants.c:142
msgid "Silence"
msgstr "Tyla"

#: ../gstreamer-properties/pipeline-constants.c:149
msgid "MJPEG (e.g. Zoran v4l device)"
msgstr "MJPEG (pvz., Zoran v4l įrenginys)"

#: ../gstreamer-properties/pipeline-constants.c:151
msgid "QCAM"
msgstr "QCAM"

#: ../gstreamer-properties/pipeline-constants.c:153
msgid "Test Input"
msgstr "Bandomoji įvestis"

#: ../gstreamer-properties/pipeline-constants.c:155
msgid "Video for Linux (v4l)"
msgstr "Video for Linux (v4l)"

#: ../gstreamer-properties/pipeline-constants.c:157
msgid "Video for Linux 2 (v4l2)"
msgstr "Video for Linux 2 (v4l2)"

#: ../gstreamer-properties/pipeline-tests.c:176
#, c-format
msgid "Failed to construct test pipeline for '%s'"
msgstr "Nepavyko „%s“ sukurti bandomojo konvejerio"

#: ../sound-theme/gvc-sound-theme-chooser.c:321
#: ../sound-theme/gvc-sound-theme-editor.c:522
msgid "No sounds"
msgstr "Jokių garsų"

#: ../sound-theme/gvc-sound-theme-chooser.c:443
msgid "Built-in"
msgstr "Įtaisytasis"

#: ../sound-theme/gvc-sound-theme-chooser.c:688
#: ../sound-theme/gvc-sound-theme-chooser.c:700
#: ../sound-theme/gvc-sound-theme-chooser.c:712
#: ../sound-theme/gvc-sound-theme-editor.c:560
#: ../sound-theme/gvc-sound-theme-editor.c:949
msgid "Sound Preferences"
msgstr "Garso nustatymai"

#: ../sound-theme/gvc-sound-theme-chooser.c:691
#: ../sound-theme/gvc-sound-theme-chooser.c:702
#: ../sound-theme/gvc-sound-theme-chooser.c:714
#: ../sound-theme/gvc-sound-theme-editor.c:562
#: ../sound-theme/gvc-sound-theme-editor.c:951
msgid "Testing event sound"
msgstr "Bandomasis įvykio signalas"

#: ../sound-theme/gvc-sound-theme-chooser.c:819
msgid "From theme"
msgstr "Iš temos"

#: ../sound-theme/gvc-sound-theme-chooser.c:828
msgid "Name"
msgstr "Vardas"

#: ../sound-theme/gvc-sound-theme-chooser.c:835
msgid "Type"
msgstr "Tipas"

#: ../sound-theme/gvc-sound-theme-chooser.c:1039
msgid "Sound _theme:"
msgstr "Garsų _tema:"

#: ../sound-theme/gvc-sound-theme-chooser.c:1047
msgid "C_hoose an alert sound:"
msgstr "Pasi_rinkite įspėjimo garsą:"

#: ../sound-theme/gvc-sound-theme-chooser.c:1078
msgid "Enable _window and button sounds"
msgstr "Įjungti _langų ir mygtukų garsus"

#. Bell
#: ../sound-theme/gvc-sound-theme-editor.c:79
msgctxt "Sound event"
msgid "Alert sound"
msgstr "Įspėjimo garsas"

#. Windows and buttons
#: ../sound-theme/gvc-sound-theme-editor.c:81
msgctxt "Sound event"
msgid "Windows and Buttons"
msgstr "Langai ir mygtukai"

#: ../sound-theme/gvc-sound-theme-editor.c:82
msgctxt "Sound event"
msgid "Button clicked"
msgstr "Spustelėtas mygtukas"

#: ../sound-theme/gvc-sound-theme-editor.c:83
msgctxt "Sound event"
msgid "Toggle button clicked"
msgstr "Spustelėtas perjungimo mygtukas"

#: ../sound-theme/gvc-sound-theme-editor.c:84
msgctxt "Sound event"
msgid "Window maximized"
msgstr "Langas išdidintas"

#: ../sound-theme/gvc-sound-theme-editor.c:85
msgctxt "Sound event"
msgid "Window unmaximized"
msgstr "Langas grąžintas iš išdidinimo"

#: ../sound-theme/gvc-sound-theme-editor.c:86
msgctxt "Sound event"
msgid "Window minimised"
msgstr "Langas sumažintas"

#. Desktop
#: ../sound-theme/gvc-sound-theme-editor.c:88
msgctxt "Sound event"
msgid "Desktop"
msgstr "Darbastalis"

#: ../sound-theme/gvc-sound-theme-editor.c:89
msgctxt "Sound event"
msgid "Login"
msgstr "Prisijungimas"

#: ../sound-theme/gvc-sound-theme-editor.c:90
msgctxt "Sound event"
msgid "Logout"
msgstr "Atsijungimas"

#: ../sound-theme/gvc-sound-theme-editor.c:91
msgctxt "Sound event"
msgid "New e-mail"
msgstr "Naujas el. laiškas"

#: ../sound-theme/gvc-sound-theme-editor.c:92
msgctxt "Sound event"
msgid "Empty trash"
msgstr "Šiukšlinės išvalymas"

#: ../sound-theme/gvc-sound-theme-editor.c:93
msgctxt "Sound event"
msgid "Long action completed (download, CD burning, etc.)"
msgstr "Baigtas ilgai trukęs veiksmas (atsiuntimas, CD įrašymas ir pan.)"

#. Alerts?
#: ../sound-theme/gvc-sound-theme-editor.c:95
msgctxt "Sound event"
msgid "Alerts"
msgstr "Įspėjimai"

#: ../sound-theme/gvc-sound-theme-editor.c:96
msgctxt "Sound event"
msgid "Information or question"
msgstr "Pranešimas arba klausimas"

#: ../sound-theme/gvc-sound-theme-editor.c:97
msgctxt "Sound event"
msgid "Warning"
msgstr "Perspėjimas"

#: ../sound-theme/gvc-sound-theme-editor.c:98
msgctxt "Sound event"
msgid "Error"
msgstr "Klaida"

#: ../sound-theme/gvc-sound-theme-editor.c:99
msgctxt "Sound event"
msgid "Battery warning"
msgstr "Išsikrauna akumuliatorius"

#: ../sound-theme/gvc-sound-theme-editor.c:592
msgid "Select Sound File"
msgstr "Pasirinkite garso failą"

#: ../sound-theme/gvc-sound-theme-editor.c:603
msgid "Sound files"
msgstr "Garso failai"

#: ../sound-theme/gvc-sound-theme-editor.c:895
#: ../sound-theme/gvc-sound-theme-editor.c:991
msgid "Disabled"
msgstr "Išjungta"

#: ../sound-theme/gvc-sound-theme-editor.c:899
msgid "Custom…"
msgstr "Pasirinktinis…"

#: ../sound-theme/gvc-sound-theme-editor.c:1302
msgid "Sound Theme:"
msgstr "Garsų tema:"

#: ../sound-theme/gvc-sound-theme-editor.c:1330
msgid "Enable window and button sounds"
msgstr "Įjungti langų ir mygtukų garsus"

#. Translators: This is the name of an audio file that sounds like the bark of a dog.
#. You might want to translate it into the equivalent words of your language.
#: ../sound-theme/sounds/gnome-sounds-default.xml.in.in.h:3
msgid "Bark"
msgstr "Lojimas"

#. Translators: This is the name of an audio file that sounds like a water drip.
#. You might want to translate it into the equivalent words of your language.
#: ../sound-theme/sounds/gnome-sounds-default.xml.in.in.h:6
msgid "Drip"
msgstr "Lašėjimas"

#. Translators: This is the name of an audio file that sounds like tapping glass.
#. You might want to translate it into the equivalent words of your language.
#: ../sound-theme/sounds/gnome-sounds-default.xml.in.in.h:9
msgid "Glass"
msgstr "Stiklas"

#. Translators: This is the name of an audio file that sounds sort of like a submarine sonar ping.
#. You might want to translate it into the equivalent words of your language.
#: ../sound-theme/sounds/gnome-sounds-default.xml.in.in.h:12
msgid "Sonar"
msgstr "Sonaras"

#~ msgid "Show desktop volume control"
#~ msgstr "Rodyti darbo aplinkos garsumo valdiklį"

#~ msgid "Sound"
#~ msgstr "Garsas"

#~ msgid "Enable debugging code"
#~ msgstr "Įjungti derinimo veikseną"

#~ msgid "Version of this application"
#~ msgstr "Programos versija"

#~ msgid " — GNOME Volume Control Applet"
#~ msgstr " — GNOME garsumo valdymo įtaisas"

#~ msgid "Waiting for sound system to respond"
#~ msgstr "Laukiama garsų sistemos atsakymo"

#~ msgid " — GNOME Volume Control"
#~ msgstr " — GNOME garsumo valdymas"

#~ msgid "Output"
#~ msgstr "Išvestis"

#~ msgid "Sound Output Volume"
#~ msgstr "Išvesties garsumas"

#~ msgid "Input"
#~ msgstr "Įvestis"

#~ msgid "Microphone Volume"
#~ msgstr "Mikrofono garsumas"
#~ msgctxt "balance"

#~ msgid "Left"
#~ msgstr "Kairė"
#~ msgctxt "balance"

#~ msgid "Right"
#~ msgstr "Dešinė"
#~ msgctxt "balance"

#~ msgid "Rear"
#~ msgstr "Galas"
#~ msgctxt "balance"

#~ msgid "Front"
#~ msgstr "Priekis"
#~ msgctxt "balance"

#~ msgid "Minimum"
#~ msgstr "Minimumas"
#~ msgctxt "balance"

#~ msgid "Maximum"
#~ msgstr "Maksimumas"

#~ msgid "_Balance:"
#~ msgstr "_Balansas:"

#~ msgid "_Fade:"
#~ msgstr "_Perėjimas:"

#~ msgid "_Subwoofer:"
#~ msgstr "_Žemų dažnių garsiakalbis:"
#~ msgctxt "volume"

#~ msgid "100%"
#~ msgstr "100%"
#~ msgctxt "volume"

#~ msgid "Unamplified"
#~ msgstr "Nesustiprintas"

#~ msgid "Mute"
#~ msgstr "Nutildyti"

#~ msgid "_Profile:"
#~ msgstr "_Profilis:"

#~ msgid "%u Output"

#~ msgid_plural "%u Outputs"
#~ msgstr[0] "%u išvestis"
#~ msgstr[1] "%u išvestys"
#~ msgstr[2] "%u išvesčių"

#~ msgid "%u Input"

#~ msgid_plural "%u Inputs"
#~ msgstr[0] "%u įvestis"
#~ msgstr[1] "%u įvestys"
#~ msgstr[2] "%u įvesčių"

#~ msgid "System Sounds"
#~ msgstr "Sistemos garsai"

#~ msgid "Co_nnector:"
#~ msgstr "J_ungtis:"

#~ msgid "Peak detect"
#~ msgstr "Maksimalaus garsumo aptikimas"

#~ msgid "Device"
#~ msgstr "Įrenginys"

#~ msgid "Speaker Testing for %s"
#~ msgstr "%s garsiakalbių tikrinimas"

#~ msgid "Test Speakers"
#~ msgstr "Tikrinti garsiakalbius"

#~ msgid "_Output volume: "
#~ msgstr "Išvesties _garsumas: "

#~ msgid "Sound Effects"
#~ msgstr "Garso efektai"

#~ msgid "_Alert volume: "
#~ msgstr "Įspėjimo _garsumas: "

#~ msgid "Hardware"
#~ msgstr "Įrenginiai"

#~ msgid "C_hoose a device to configure:"
#~ msgstr "Pasi_rinkite derintiną įrenginį:"

#~ msgid "Settings for the selected device:"
#~ msgstr "Pasirinkto įrenginio nustatymai:"

#~ msgid "_Input volume: "
#~ msgstr "Į_vesties garsumas: "

#~ msgid "Input level:"
#~ msgstr "Įvesties lygis:"

#~ msgid "C_hoose a device for sound input:"
#~ msgstr "Pasi_rinkite garso įvesties įrenginį:"

#~ msgid "C_hoose a device for sound output:"
#~ msgstr "Pasi_rinkite garso išvesties įrenginį:"

#~ msgid "Applications"
#~ msgstr "Programos"

#~ msgid "No application is currently playing or recording audio."
#~ msgstr "Šiuo metu jokia programa negroja ir neįrašinėja garso."

#~ msgid "Stop"
#~ msgstr "Stabdyti"

#~ msgid "Test"
#~ msgstr "Tikrinti"

#~ msgid "Subwoofer"
#~ msgstr "Žemų dažnių garsiakalbis"

#~ msgid "Failed to start Sound Preferences: %s"
#~ msgstr "Nepavyko atverti garso nustatymų: %s"

#~ msgid "_Mute"
#~ msgstr "_Nutildyti"

#~ msgid "_Sound Preferences"
#~ msgstr "_Garso nustatymai"

#~ msgid "Muted"
#~ msgstr "Nutildyta"

#~ msgid "Untitled-%d"
#~ msgstr "Nepavadinta-%d"

#~ msgid "%s (%llu byte)"

#~ msgid_plural "%s (%llu bytes)"
#~ msgstr[0] "%s (%llu baitas)"
#~ msgstr[1] "%s (%llu baitai)"
#~ msgstr[2] "%s (%llu baitų)"

#~ msgid "There was an error displaying help: %s"
#~ msgstr "Rodant žinyną įvyko klaida: %s"

#~ msgid "Editing profile \"%s\""
#~ msgstr "Taisomas profilis „%s“"

#~ msgid "<no name>"
#~ msgstr "<nepavadintas>"

#~ msgid "<no description>"
#~ msgstr "<nėra aprašymo>"

#~ msgid "There was an error getting the list of gm_audio profiles. (%s)\n"
#~ msgstr "Gaunant gm_audio profilių sąrašą įvyko klaida. (%s)\n"

#~ msgid ""
#~ "There was an error subscribing to notification of audio profile list "
#~ "changes. (%s)\n"
#~ msgstr ""
#~ "Įvyko klaida registruojantis prie garso profilių sąrašo pakeitimų "
#~ "perspėjimų. (%s)\n"

#~ msgid "There was an error forgetting profile path %s. (%s)\n"
#~ msgstr "Pamirštant profilio kelią %s, įvyko klaida. (%s)\n"

#~ msgid "_Edit"
#~ msgstr "_Taisyti"

#~ msgid "Delete this profile?\n"

#~ msgid_plural "Delete these %d profiles?\n"
#~ msgstr[0] "Pašalinti šį %d profilį?\n"
#~ msgstr[1] "Pašalinti šiuos %d profilius?\n"
#~ msgstr[2] "Pašalinti šiuos %d prafilių?\n"

#~ msgid "Delete profile \"%s\"?"
#~ msgstr "Ištrinti profilį „%s“?"

#~ msgid "Delete Profile"
#~ msgstr "Trinti profilį"

#~ msgid "Edit GNOME Audio Profiles"
#~ msgstr "Taisyti GNOME garso profilius"

#~ msgid "_Profiles:"
#~ msgstr "_Profiliai:"

#~ msgid "You already have a profile called \"%s\""
#~ msgstr "Jau yra profilis pavadinimu „%s“"

#~ msgid "GConf Error (FIXME): %s\n"
#~ msgstr "GConf klaida (FIXME): %s\n"

#~ msgid ""
#~ "The file \"%s\" is missing. This indicates that the application is "
#~ "installed incorrectly, so the dialog can't be displayed."
#~ msgstr ""
#~ "Trūksta failo „%s“. Tai rodo, kad programa yra įdiegta netinkamai, taigi "
#~ "dialogas negali būti parodytas."

#~ msgid ""
#~ "%s\n"
#~ "Run '%s --help' to see a full list of available command line options.\n"
#~ msgstr ""
#~ "%s\n"
#~ "Paleiskite „%s --help“, jei norite susipažinti su visais galimais "
#~ "komandinės eilutės parametrais.\n"

#~ msgid "Edit Audio Profile"
#~ msgstr "Taisyti garso profilį"

#~ msgid "Profile _description:"
#~ msgstr "Profilio _aprašymas:"

#~ msgid "_Active?"
#~ msgstr "_Aktyvus?"

#~ msgid "_File extension:"
#~ msgstr "Failo _plėtinys:"

#~ msgid "_GStreamer pipeline:"
#~ msgstr "_GStreamer konvejeris:"

#~ msgid "_Profile name:"
#~ msgstr "_Profilio vardas:"

#~ msgid "New Profile"
#~ msgstr "Naujas profilis"

#~ msgid "_Create"
#~ msgstr "_Kurti"

#~ msgid "A description for the audio profile"
#~ msgstr "Garso profilio aprašymas"

#~ msgid ""
#~ "A description of the profile, containing more information and describing "
#~ "when to use this profile."
#~ msgstr ""
#~ "Profilio aprašymas, kuriame yra daugiau informacijos ir aprašymas kada "
#~ "naudoti šį profilį."

#~ msgid ""
#~ "A filename extension to be used when storing files encoded with this "
#~ "profile."
#~ msgstr ""
#~ "Failo plėtinys, naudojamas, kai saugomi failai koduoti pagal šį profilį."

#~ msgid "A partial GStreamer pipeline to use for this profile."
#~ msgstr "Dalinis GStreamer konvejeris skirtas naudoti su šiuo profiliu."

#~ msgid "A short name for the audio profile"
#~ msgstr "Trumpas garso profilio vardas"

#~ msgid ""
#~ "A short name for the audio profile, to be used in selections and uniquely "
#~ "identifying the profile."
#~ msgstr ""
#~ "Trumpas garso profilio vardas, naudojamas parinkimui ir profilio "
#~ "įvardijimui."

#~ msgid "CD Quality, AAC"
#~ msgstr "CD kokybės, AAC"

#~ msgid "CD Quality, Lossless"
#~ msgstr "CD kokybės, nenuostolingas"

#~ msgid "CD Quality, Lossy"
#~ msgstr "CD kokybės, nuostolingas"

#~ msgid "CD Quality, MP2"
#~ msgstr "CD kokybės, MP2"

#~ msgid "CD Quality, MP3"
#~ msgstr "CD kokybės, MP3"

#~ msgid ""
#~ "List of audio recording profiles. The list contains strings naming "
#~ "subdirectories relative to /system/gstreamer/@GST_MAJORMINOR@/audio/"
#~ "profiles."
#~ msgstr ""
#~ "Garso įrašymo profilių sąrašas. Šiame sąraše yra eilutės, nurodančios "
#~ "poaplankius, santykinius /system/gstreamer/@GST_MAJORMINOR@/audio/"
#~ "profiles."

#~ msgid "List of profiles"
#~ msgstr "Profilių sąrašas"

#~ msgid "The default file extension for this profile"
#~ msgstr "Numatytasis šio profilio failų plėtinys"

#~ msgid "The partial GStreamer pipeline used"
#~ msgstr "Dalinis naudotas GStreamer konvejeris"

#~ msgid ""
#~ "Used for converting to CD-quality audio, but with a lossless compression "
#~ "codec. Use this if you later want to edit the file or burn it to CD."
#~ msgstr ""
#~ "Naudojama konvertuojant CD kokybės garsą, tačiau su nenuostolingu "
#~ "glaudinimu. Naudokit šį profilį, jei vėliau norėsite taisyti šį failą "
#~ "arba norėsite įrašyti jį į CD."

#~ msgid ""
#~ "Used for converting to CD-quality audio, but with a lossy compression "
#~ "codec. Use this for CD extraction and radio recordings."
#~ msgstr ""
#~ "Naudojama konvertuojant CD kokybės garsą, tačiau su nuostolingu "
#~ "glaudinimo kodeku. Naudokite šį profilį garso rašymui iš CD ir radijo "
#~ "įrašams."

#~ msgid ""
#~ "Used for converting to CD-quality audio, but with the lossy AAC codec. "
#~ "Use this for preparing files for copying to devices that only support the "
#~ "AAC codec. Note that using this format may be illegal in your "
#~ "jurisdiction; contact your lawyer for advice."
#~ msgstr ""
#~ "Naudojama konvertuojant CD kokybės garsą, tačiau su nuostolingu AAC "
#~ "kodeku. Naudokite šį profilį failų, kuriuos kopijuosite į įrenginius, "
#~ "palaikančius tik AAC kodeką, paruošimui. Atminkite, kad naudoti šį "
#~ "formatą jūsų teisės sistemoje gali būti nelegalu; pasitarkite su savo "
#~ "teisininku."

#~ msgid ""
#~ "Used for converting to CD-quality audio, but with the lossy MP2 codec. "
#~ "Use this for preparing files for copying to devices that only support the "
#~ "MP2 codec. Note that using this format may be illegal in your "
#~ "jurisdiction; contact your lawyer for advice."
#~ msgstr ""
#~ "Naudojama konvertuojant CD kokybės garsą, tačiau su nuostolingu MP2 "
#~ "kodeku. Naudokite šį profilį failų, kuriuos kopijuosite į įrenginius, "
#~ "palaikančius tik MP2 kodeką, paruošimui. Atminkite, kad naudoti šį "
#~ "formatą jūsų teisės sistemoje gali būti nelegalu; pasitarkite su savo "
#~ "teisininku."

#~ msgid ""
#~ "Used for converting to CD-quality audio, but with the lossy MP3 codec. "
#~ "Use this for preparing files for copying to devices that only support the "
#~ "MP3 codec. Note that using this format may be illegal in your "
#~ "jurisdiction; contact your lawyer for advice."
#~ msgstr ""
#~ "Naudojama konvertuojant CD kokybės garsą, tačiau su nuostolingu MP3 "
#~ "kodeku. Naudokite šį profilį failų, kuriuos kopijuosite į įrenginius, "
#~ "palaikančius tik MP3 kodeką, paruošimui. Atminkite, kad naudoti šį "
#~ "formatą jūsų teisės sistemoje gali būti nelegalu; pasitarkite su savo "
#~ "teisininku."

#~ msgid ""
#~ "Used for converting to lossless voice-quality audio. Use this for "
#~ "recording and editing speech."
#~ msgstr ""
#~ "Naudojama konvertuojant į nenuostolingą balso kokybės garsą. Naudokite šį "
#~ "profilį kalbos įrašymui ir taisymui."

#~ msgid ""
#~ "Used for converting to lossy voice-quality audio. Use this for recording "
#~ "speech that doesn't need to be edited."
#~ msgstr ""
#~ "Naudojama konvertuojant į nuostolingą balso kokybės garsą. Naudokite šį "
#~ "profilį kalbos, kurios nereiks taisyti, įrašymui."

#~ msgid "Voice, Lossless"
#~ msgstr "Balsas, nenuostolingas"

#~ msgid "Voice, Lossy"
#~ msgstr "Balsas, nuostolingas"

#~ msgid "Whether this profile is to be used"
#~ msgstr "Ar naudotinas šis profilis"

#~ msgid "Whether to use and display this profile."
#~ msgstr "Ar rodyti ir naudoti šį profilį."

#~ msgid "Recording..."
#~ msgstr "Įrašoma..."

#~ msgid "Custom..."
#~ msgstr "Pasirinktinis..."
